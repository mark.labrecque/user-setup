#!/bin/bash

# Add repos
sudo apt-key --keyring /usr/share/keyrings/1password.gpg adv --keyserver keyserver.ubuntu.com --recv-keys 3FEF9748469ADBE15DA7CA80AC2D62742012EA22
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/1password.gpg] https://downloads.1password.com/linux/debian edge main' | sudo tee /etc/apt/sources.list.d/1password.list
sudo add-apt-repository ppa:appimagelauncher-team/stable

# Docker
echo "Setting up Docker..."
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt update
sudo apt install -y code docker-ce docker-compose 1password vim zsh appimagelauncher gnome-tweaks gnome-backgrounds
flatpak install slack telegram bitwarden todoist -y

# and post-install
sudo usermod -aG docker $USER
echo "Docker has been setup. Please restart your system so permissions are set."

# Changing Linux time to local time
timedatectl set-local-rtc 1 --adjust-system-clock

# Dev environment
echo "Setting up dev environment..."
sudo apt install -y php7.4-cli php7.4-dom php7.4-mbstring php7.4-xml php7.4-gd php7.4-curl php7.4-json php7.4-zip -y
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
composer
rm composer-setup.php
echo "Dev environment has been set up."

# Setup Git configuration
git config --global alias.st status
git config --global alias.br branch
git config --global alias.co checkout
git config --global user.name "Mark Labrecque"
git config --global user.email mark@affinitybridge.com

# Enable 3-finger drag
#sudo cp libinput.so.10.14.0 /usr/lib/x86_64-linux-gnu/
#sudo ln -sf /usr/lib/x86_64-linux-gnu/libinput.so.10.14.0 /usr/lib/x86_64-linux-gnu/libinput.so.10

# Install ansible
sudo apt update && sudo apt install python3-pip
sudo python3 -m pip install ansible

# Create default folders
mkdir ~/Applications
mkdir ~/Projects

# Install Openconnect VPN for BCCHR.
sudo apt install openconnect

# Install ssh-keys (need to be downloaded)
mv ~/Downloads/id_rsa ~/.ssh/id_rsa && chmod 600 ~/.ssh/id_rsa
mv ~/Downloads/id_rsa.pub ~/.ssh/id_rsa.pub && chmod 644 ~/.ssh/id_rsa.pub

# Setup global install of node/npm
tar -zxvf nodejs.tar.gz
sudo mv nodejs /usr/local/etc/
cd /usr/local/bin
ln -s node /usr/local/etc/nodejs/bin/node
ln -s npm /usr/local/etc/nodejs/lib/node_modules/npm/bin/npm-cli.js
ln -s npx /usr/local/etc/nodejs/lib/node_modules/npm/bin/npx-cli.js

# Install nativefier globally
npm install -g nativefier

# Move web apps and shortcuts into place
cd ~/.ssh
mv apps/* ~/Applications/
mv ~/.ssh/shortcuts/* ~/.local/share/applications/


# Install oh-my-zsh
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Setup Windows dual-boot
# echo "TO DUAL BOOT WINDOWS: sudo cp -r /<windows-efi-path>/EFI/Microsoft /boot/efi/EFI"

# Add line to bash config, alias bcchr-vpn="sudo openconnect -b --protocol=gp remote.bcchr.ca"
# And to kill the VPN, alias vpn-kill="sudo killall -SIGINT openconnect"
