#!/bin/bash

sudo dpkg --add-architecture i386
wget -nc https://dl.winehq.org/wine-builds/winehq.key
sudo apt-key add winehq.key
# 20.04 sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ focal main'
sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ groovy main'
sudo apt update && sudo apt install --install-recommends winehq-staging -y
sudo apt install lutris -y