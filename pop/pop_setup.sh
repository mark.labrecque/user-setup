#!/bin/bash

# Changing Linux time to local time
timedatectl set-local-rtc 1 --adjust-system-clock

# Apply updates
sudo apt update -y && sudo apt upgrade -y

# Add FreeType font livrary support
sudo apt install libfreetype6:i386

# Add repos
sudo add-apt-repository ppa:appimagelauncher-team/stable
sudo add-apt-repository ppa:ondrej/php

# Docker
echo "Setting up Docker..."
sudo apt update
sudo apt install libnss3-tools apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key --keyring /etc/apt/trusted.gpg.d/docker-apt-key.gpg add
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu jammy stable"
sudo apt update
sudo apt install -y code docker-ce docker-compose vim zsh appimagelauncher gnome-tweaks gnome-backgrounds
flatpak install slack bitwarden -y

# and post-install
sudo usermod -aG docker $USER
echo "Docker has been setup. Please restart your system so permissions are set."

# Dev environment
echo "Setting up dev environment..."
#sudo apt-get -o Dpkg::Options::="--force-overwrite" install libpcre2-posix2
sudo apt update
sudo apt install -f
sudo apt upgrade
#sudo add-apt-repository ppa:ondrej/php
sudo apt update -y
#sudo apt install -y php8.0-fpm php8.0-cli php8.0-dom php8.0-gd php8.0-mbstring php8.0-curl nodejs npm -y
sudo apt install -y php8.1-fpm php8.1-cli php8.1-dom php8.1-gd php8.1-mbstring php8.1-curl nodejs npm -y
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
composer
rm composer-setup.php
echo "Dev environment has been set up."

# Setup Git configuration
git config --global alias.st status
git config --global alias.br branch
git config --global alias.co checkout
git config --global user.name "Mark Labrecque"
git config --global user.email mark@affinitybridge.com

# Enable 3-finger drag
#sudo cp libinput.so.10.14.0 /usr/lib/x86_64-linux-gnu/
#sudo ln -sf /usr/lib/x86_64-linux-gnu/libinput.so.10.14.0 /usr/lib/x86_64-linux-gnu/libinput.so.10

# Install ansible
#sudo apt update && sudo apt install python3-pip
#sudo python3 -m pip install ansible

# Create default folders
#mkdir ~/Applications
mkdir ~/Projects
#mv ../template.desktop ~/Templates/
#cp ../apps/* ~/Applications/

# Install Openconnect VPN for BCCHR.
sudo apt install openconnect

# Install ssh-keys (need to be downloaded)
mv ~/Downloads/id_rsa ~/.ssh/id_rsa && chmod 600 ~/.ssh/id_rsa
mv ~/Downloads/id_rsa.pub ~/.ssh/id_rsa.pub && chmod 644 ~/.ssh/id_rsa.pub

# Install DDEV
# Add DDEV’s GPG key to your keyring
sudo sh -c 'echo ""'
sudo apt update && sudo apt install -y curl
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://pkg.ddev.com/apt/gpg.key | gpg --dearmor | sudo tee /etc/apt/keyrings/ddev.gpg > /dev/null
sudo chmod a+r /etc/apt/keyrings/ddev.gpg

# Add DDEV releases to your package repository
sudo sh -c 'echo ""'
echo "deb [signed-by=/etc/apt/keyrings/ddev.gpg] https://pkg.ddev.com/apt/ * *" | sudo tee /etc/apt/sources.list.d/ddev.list >/dev/null

# Update package information and install DDEV
sudo sh -c 'echo ""'
sudo apt update && sudo apt install -y ddev
mkcert -install

# Install oh-my-zsh
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Setup Windows dual-boot
# echo "TO DUAL BOOT WINDOWS: sudo cp -r /<windows-efi-path>/EFI/Microsoft /boot/efi/EFI"

# Add line to bash config, alias bcchr-vpn="sudo openconnect -b --protocol=gp remote.bcchr.ca"
# And to kill the VPN, alias vpn-kill="sudo killall -SIGINT openconnect"
