#!/bin/bash

sudo apt update
sudo apt upgrade -y

# Set locale
sudo update-locale LANG=en_US.UTF8

# Install ssh-keys (need to be downloaded, and in ~/.ssh)
sudo chown mark:mark ~/.ssh/id_rsa*
chmod 600 ~/.ssh/id_rsa
chmod 644 ~/.ssh/id_rsa.pub

# Setup Git configuration
git config --global alias.st status
git config --global alias.br branch
git config --global alias.co checkout
git config --global user.name "Mark Labrecque"
git config --global user.email mark@affinitybridge.com

# Install PHP
sudo apt install openconnect -y

# Setup folders
mkdir ~/Projects

# Install additional tools
sudo apt install vim zsh -y

# Install developer tools
sudo add-apt-repository ppa:ondrej/php
sudo apt install -y php8.2-fpm php8.2-cli php8.2-dom php8.2-gd php8.2-zip nodejs npm zip -y
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
composer
rm composer-setup.php
echo "Dev environment has been set up."

# Install oh-my-zsh
sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"

# Add line to bash config, alias bcchr-vpn="sudo openconnect -b --protocol=gp remote.bcchr.ca"
# And to kill the VPN, alias vpn-kill="sudo killall -SIGINT openconnect"
