#!/bin/bash

# Copy over and correct permissions on ssh keys
cp ~/Downloads/id_rsa ~/.ssh/id_rsa && chmod 600 ~/.ssh/id_rsa
cp ~/Downloads/id_rsa.pub ~/.ssh/id_rsa.pub && chmod 644 ~/.ssh/id_rsa.pub

# Setup homebrew (x86_64 installation)
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
# This line is required for M1 support, but won't break Intel-based
export PATH="$PATH:/opt/homebrew/bin"

# Move keys from Downloads to .ssh
mv ~/Downloads/id_rsa . && chmod 600 id_rsa
mv ~/Downloads/id_rsa.pub . && chmod 644 id_rsa.pub

# Setup devy
git clone gitlab-ab:affinitybridge/dev-playbook ~/dev-playbook

# Install Composer (use OSX prebuilt php)
cd ~/
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php --filename=composer --install-dir=/usr/local/bin
rm composer-setup.php

# Setup Git configuration
git config --global alias.st status
git config --global alias.br branch
git config --global alias.co checkout
git config --global user.name "Mark Labrecque"
git config --global user.email mark@affinitybridge.com

# Install vim and oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"