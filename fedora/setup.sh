#!/bin/bash

git config --global alias.br branch
git config --global alias.co checkout
git config --global alias.st status
git config --global user.mail mark@affinitybridge.com
git config --global user.name "Mark Labrecque"

# Add VS Code remote/repo
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'

# Misc setup
sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf update -y
sudo dnf install ulauncher gnome-tweaks vim code ffmpeg-libs -y

# Setup flathub remote
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Setup / install Docker
sudo dnf -y install dnf-plugins-core
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
sudo dnf install docker-ce docker-ce-cli containerd.io docker-compose-plugin
sudo systemctl start docker
sudo docker run hello-world
sudo groupadd docker
sudo usermod -aG docker $USER
sudo dnf install starship zsh -y
sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"

# Setup flatpak / remotes
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install slack bitwarden -y

# Install VS Code / remotes
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
dnf check-update
sudo dnf install code -y

# Install development toolchain
curl -LO https://raw.githubusercontent.com/drud/ddev/master/scripts/install_ddev.sh && bash install_ddev.sh
mkcert -install
sudo dnf -y install https://rpms.remirepo.net/fedora/remi-release-36.rpm
sudo dnf install php7.4 php-gd
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
mkdir ~/{bin,Projects}
php composer-setup.php --install-dir=/home/mark/bin --filename=composer
composer
rm composer-setup.php

# Move keys over from ~/Downloads
mv ~/Downloads/id_rsa* ~/.ssh/
chmod 600 ~/.ssh/id_rsa
chmod 644 ~/.ssh/id_rsa.pub
sudo chmod 755 ~/.ssh/config
sudo chmod -R 755 ~/.ssh/config.d


